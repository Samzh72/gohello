package utils

import (
	"fmt"
	"os"
	"runtime"
	"time"
)

const (
	LogLevelFatal = iota
	LogLevelError
	LogLevelInfo
	LogLevelAll
)

type logModule struct {
	prefix string
	level  int
	time   bool
}

var logModules map[string]*logModule
var globalLogModule = logModule{"", LogLevelError, false}

func log(lead string, level int, format string, v ...interface{}) {
	_, file, _, _ := runtime.Caller(2)
	module, ok := logModules[file]
	if !ok {
		module = &globalLogModule
	}

	if module.level < level {
		return
	}

	if module.time {
		t := time.Now().Format("15:04:05.000000")
		fmt.Printf(lead+" "+t+" "+format+"\n", v...)
	} else {
		fmt.Printf(lead+" "+format+"\n", v...)
	}
}

// LogFatal will log the message and then print call stack
// and terminate the process.
func LogFatal(format string, v ...interface{}) {
	log("[F]", LogLevelFatal, format, v...)
	// make 10K space call stack, anyway we're exiting...
	stack := make([]byte, 10*1024)
	runtime.Stack(stack, true)
	fmt.Println("Call Stack")
	fmt.Println(string(stack))
	os.Exit(1)
}

// LogError will log error messages. error means there's something
// wrong, but the program could still continue.
func LogError(format string, v ...interface{}) {
	log("[E]", LogLevelError, format, v...)
}

// LogInfo will log info message. info means it's just help for
// debugging or diagnositic.
func LogInfo(format string, v ...interface{}) {
	log("[I]", LogLevelInfo, format, v...)
}

// LogCheckpoint will log the caller's file and line number. this
// function should not be called in release build, just a debug
// helper.
func LogCheckpoint() {
	_, file, line, _ := runtime.Caller(1)
	fmt.Printf("[C] %4d:%s", line, file)
}

// LogRegisterModule could be used to customize the behavior
// within a 'module', that is a single GO file.
// It should be called in init() of the GO file.
// If it is not called, a default log profile will be taken:
//      prefix: ""
//      level: LogLevelFatal
//      time: false
// prefix is an special tag for the module.
// level defines the minimum log level, any log higher than this
// value will be ignored. for example:
//      level = LogLevelError
//      LogFatal(), LogError will output normally
//      LogInfo() will be suppressed
// if time is true, then the log will comes with timestamp
func LogRegisterModule(prefix string, level int, time bool) {
	_, file, _, _ := runtime.Caller(1)
	logModules[file] = &logModule{prefix, level, time}
}

func init() {
	logModules = make(map[string]*logModule)
}
