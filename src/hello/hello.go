package hello

import (
	"fmt"
)

type Handler func()

type TestSuite interface {
	RegisterKeyword(k string, h Handler)
	GetHandlerByKeyword(k string) (Handler, bool)
	ListKeywords()
}

type HelloTestSuite struct {
	handlers map[string]Handler
}

func (self *HelloTestSuite) Init() {
	self.handlers = make(map[string]Handler)
}

func (self *HelloTestSuite) RegisterKeyword(k string, h Handler) {
	self.handlers[k] = h
}

func (self HelloTestSuite) GetHandlerByKeyword(k string) (Handler, bool) {
	h, ok := self.handlers[k]
	return h, ok
}

func (self HelloTestSuite) ListKeywords() {
	for k := range self.handlers {
		fmt.Println("\t" + k)
	}
}

func GetTestCaseHeaderFunc() func() {
	caseIndex := 0
	return func() {
		fmt.Printf("======== Case %02d ========\n", caseIndex)
		caseIndex++
	}
}
