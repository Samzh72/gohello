package pkg

import (
	"fmt"
	"hello"
	"sync"
	"sync/atomic"
	"time"
)

var pheader func()

func mutexTest() {
	pheader()
	// mutex in Go is not linux mutex, because linux mutex is a thread lock
	// but Go need a lock on goroutine level
	//
	// the mutex implementation is a combination of a Go semaphore and a
	// counter. the semaphore is implemented as:
	//
	// runtime.semacquire(),    acquire a semaphore
	// runtime.parkunlock(),    wrapper of runtime.park()
	// runtime.park(),          put current goroutine into wait state
	// runtime.park0(),         execute(gp) is called to schedule
	//
	// runtime.semrelease()
	// runtime.ready(),         make related goroutines to ready state

	// there's no much cases for mutex test
	t := time.Now()
	m := new(sync.Mutex)
	m.Lock()
	go func() {
		m.Lock()
		fmt.Println("time elapse should be 2s: ", time.Since(t))
		m.Unlock()
	}()
	time.Sleep(2 * time.Second)
	m.Unlock()
	time.Sleep(1 * time.Millisecond) // trigger scheduler
}

func rwLockTest() {
	var wg sync.WaitGroup

	pheader()
	// RLock() will not block other RLock()
	// RUnlock() could happen in another goroutine
	// lock/unlock have to be paired
	var rw sync.RWMutex
	rw.RLock()
	wg.Add(1)
	go func() {
		rw.RLock()
		rw.RUnlock()
		rw.RUnlock() // pair RLock() in main goroutine
		wg.Done()
	}()
	wg.Wait()

	pheader()
	// RLock() will block other Lock()
	// Lock() will block RLock() vice versus
	rw.RLock()
	wg.Add(1)
	go func() {
		t := time.Now()
		// will return after 2s
		rw.Lock()
		fmt.Println("rw.Lock() was blocked for ", time.Since(t))
		rw.Unlock()
		wg.Done()
	}()
	time.Sleep(2 * time.Second)
	rw.RUnlock()
	wg.Wait()
}

func condTest() {
	pheader()
	c := sync.NewCond(new(sync.Mutex))
	i := 10
	go func(c *sync.Cond) {
		time.Sleep(1 * time.Second)
		c.L.Lock()
		i = 0
		// unlock could be called before or after signal()
		// but before signal() is better otherwise there
		// will be another schedule happens during wait()
		// aquires lock()
		c.L.Unlock()
		c.Signal()
	}(c)

	c.L.Lock()
	for i != 0 {
		c.Wait()
	}
	c.L.Unlock()
}

func atomicTest() {
	pheader()
	// CMPX int
	i := int32(100)
	fmt.Println("before CMPX(int) ", i)
	swapped := atomic.CompareAndSwapInt32(&i, i, 101)
	fmt.Println("after CMPX ", i, " Swapped: ", swapped)
}

func syncTest() {
	mutexTest()
	rwLockTest()
	condTest()
	atomicTest()
}

func init() {
	pheader = hello.GetTestCaseHeaderFunc()
}
