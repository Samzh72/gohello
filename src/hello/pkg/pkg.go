package pkg

import (
	"hello"
)

type PkgTestSuite struct {
	hello.HelloTestSuite
}

func New() *PkgTestSuite {
	ts := new(PkgTestSuite)
	ts.Init()

	ts.RegisterKeyword("sync", syncTest)

	return ts
}
