package perf

import (
	"fmt"
	"hello"
	"regexp"
	"strings"
	"time"
)

const s0 = "abcdef"

func stringCmp(n int) {
	fmt.Print("stringCmp:\t", n, "\t")

	s1 := strings.Repeat(s0, 10)
	s2 := s1
	t := time.Now()
	for i := 0; i < n; i++ {
		if s2 != s1 {
			fmt.Println("impossible")
		}
	}
	fmt.Println(time.Since(t))
}

func regexCmp(n int) {
	fmt.Print("regexCmp:\t", n, "\t")

	s1 := strings.Repeat(s0, 10)
	re, _ := regexp.Compile("abcdef")
	t := time.Now()
	for i := 0; i < n; i++ {
		if !re.MatchString(s1) {
			fmt.Println("impossible")
		}
	}
	fmt.Println(time.Since(t))
}
func regexTest() {
	pheader := hello.GetTestCaseHeaderFunc()

	// this test shows that string compare is about 100 times faster
	// than regexp

	pheader()
	// string comparision
	stringCmp(1000)
	stringCmp(10000)
	stringCmp(100000)

	pheader()
	// regex comparision
	regexCmp(1000)
	regexCmp(10000)
	regexCmp(100000)
}
