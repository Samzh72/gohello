package perf

import (
	"hello"
)

type PerfTestSuite struct {
	hello.HelloTestSuite
}

func New() *PerfTestSuite {
	ts := new(PerfTestSuite)
	ts.Init()

	ts.RegisterKeyword("map", mapTest)
	ts.RegisterKeyword("regex", regexTest)

	return ts
}
