package perf

import (
	"fmt"
	"hello"
	"strconv"
	"time"
)

type node struct {
	i int
	s string
}

func runIntMap(n int) {
	t1 := time.Now()

	m, s := createIntMap(n)
	t2 := time.Now()

	visitIntMapByRange(m)
	t3 := time.Now()

	visitIntMapByRandom(m, s)
	t4 := time.Now()

	d1 := t2.Sub(t1)
	d2 := t3.Sub(t2)
	d3 := t4.Sub(t3)

	fmt.Println("map[int]node with ", n, "elements")
	fmt.Println("create map takes\t", d1)
	fmt.Println("range visit takes\t", d2)
	fmt.Println("random visit take\t", d3)
}

func createIntMap(n int) (map[int]node, []string) {
	m := make(map[int]node)
	slice := make([]string, n)

	var s string
	for i := 0; i < n; i++ {
		s = strconv.Itoa(i)
		slice[i] = s
		m[i] = node{i, s}
	}
	return m, slice
}

func visitIntMapByRange(m map[int]node) {
	var sum = 0
	for k := range m {
		sum += m[k].i
	}
}

func visitIntMapByRandom(m map[int]node, s []string) {
	var sum = 0
	for i, _ := range s {
		sum += m[i].i
	}
}

func runStringMap(n int) {
	t1 := time.Now()

	m, s := createStringMap(n)
	t2 := time.Now()

	visitStringMapByRange(m)
	t3 := time.Now()

	visitStringMapByRandom(m, s)
	t4 := time.Now()

	d1 := t2.Sub(t1)
	d2 := t3.Sub(t2)
	d3 := t4.Sub(t3)

	fmt.Println("map[string]node with ", n, "elements")
	fmt.Println("create map takes\t", d1)
	fmt.Println("range visit takes\t", d2)
	fmt.Println("random visit take\t", d3)
}

func createStringMap(n int) (map[string]node, []string) {
	m := make(map[string]node)
	slice := make([]string, n)

	var s string
	for i := 0; i < n; i++ {
		s = strconv.Itoa(i)
		slice[i] = s
		m[s] = node{i, s}
	}
	return m, slice
}

func visitStringMapByRange(m map[string]node) {
	var sum = 0
	for k := range m {
		sum += m[k].i
	}
}

func visitStringMapByRandom(m map[string]node, s []string) {
	var sum = 0
	for _, v := range s {
		sum += m[v].i
	}
}

func mapTest() {
	pheader := hello.GetTestCaseHeaderFunc()

	pheader()
	// int keys
	runIntMap(10000)
	runIntMap(100000)
	runIntMap(1000000)
	runIntMap(10000000)

	pheader()
	// string keys
	runStringMap(10000)
	runStringMap(100000)
	runStringMap(1000000)
	runStringMap(10000000)
}
