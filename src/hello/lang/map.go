package lang

import (
	"fmt"
	"hello"
)

func mapTest() {
	pheader := hello.GetTestCaseHeaderFunc()

	pheader()
	// declare map
	var m1 map[string]string
	fmt.Println("before make, m1 == nil ?", m1 == nil)

	pheader()
	m1 = make(map[string]string)
	fmt.Println("size of m1 is ", len(m1))
	// map is increasing automatically
	m1["a"] = "aa"
	fmt.Println("size of m1 is ", len(m1))

	pheader()
	// delete will shrink map
	delete(m1, "a")
	fmt.Println("size of m1 is ", len(m1))

	pheader()
	// map with structure as value
	type mapValue struct {
		i int
	}
	var m2 = map[string]mapValue{"a": {0}}
	fmt.Println("size of m2 is ", len(m2))
	fmt.Println("i in mapValue is ", m2["a"].i)
	// m2["a"].i = 10       // illegal assignment

	pheader()
	// map with pointer as value
	var m3 = map[string]*mapValue{"a": {0}}
	fmt.Println("size of m3 is ", len(m3))
	fmt.Println("i in mapValue is ", m3["a"].i)
	m3["a"].i = 10 // legal because value is pointer
	fmt.Println("i in mapValue is ", m3["a"].i)

	pheader()
	// map with simple types
	var m4 = map[string]int{"a": 0}
	m4["a"]++
	fmt.Println("after ++, value of m4[\"a\"] is ", m4["a"])

	pheader()
	// unsafe for concurrency
	// no good test case

	pheader()
	// unstable order
	var m5 = make(map[int]int)

	for i := 0; i < 10; i++ {
		m5[i] = 100 - i
	}

	for k, v := range m5 {
		fmt.Printf("map[%d] = %d\n", k, v)
	}
}
