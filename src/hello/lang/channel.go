package lang

import (
	"fmt"
	"hello"
	"sync"
	"time"
)

var pheader func()

func bufferedOrNot(i int, c chan int) {
	var t time.Time
	t = time.Now()
	go func() {
		fmt.Println(i, 1.1, time.Since(t))
		c <- 0
		// for buffered channel:
		//      1.2 will be printed out with 1.1 together
		// for non-buffered channel:
		//      1.2 will be blocked for 2s
		fmt.Println(i, 1.2, time.Since(t))
		c <- 0
		// same as above, will be blocked on non-bufferred channel
		fmt.Println(i, 1.3, time.Since(t))
	}()

	time.Sleep(2 * time.Second)
	<-c
	fmt.Println(i, 2.1, time.Since(t))
	time.Sleep(2 * time.Second)
	<-c
	fmt.Println(i, 2.2, time.Since(t))
}

func channelTypes() {
	// in general, data types passed through channel is just like
	// parameters passed to a function
	// some types(map, slice, channel as reference) and others as
	// value
	type channelData struct {
		i int
		s string
	}

	pheader()
	// pointer type
	// pointer is just a number, so we have the same address
	// on both side of channel
	var c1 chan *channelData
	c1 = make(chan *channelData, 5)
	data1 := &channelData{1, "a"}
	fmt.Printf("data address sent to channel = %p\n", data1)
	c1 <- data1
	data1 = <-c1
	fmt.Printf("data address received from channel = %p\n", data1)

	pheader()
	// struct type
	// channel passes VALUES, so even it is a struct, a copy of
	// the VALUE will be sent/received on both side of channel
	// the expectation of the following test is we receive a
	// unchanged value
	var c2 chan channelData
	c2 = make(chan channelData, 1)
	data2 := channelData{1, "a"}
	fmt.Println("data sent to channel = ", data2)
	c2 <- data2
	data2.i = 2
	fmt.Println("and changed to ", data2)
	data2 = <-c2
	fmt.Println("data received from channel =", data2)

	pheader()
	// map
	// map, slice, channel, string functions are reference
	// here we expect data received is the data after changed
	// even if the changing happens after sending
	var c3 chan map[int]channelData
	c3 = make(chan map[int]channelData, 1)
	data3 := make(map[int]channelData)
	data3[1] = channelData{1, "a"}
	fmt.Println("data sent to channel = ", data3)
	c3 <- data3
	data3[2] = channelData{2, "b"}
	fmt.Println("and changed to ", data3)
	data3 = <-c3
	fmt.Println("data received from channel =", data3)

	pheader()
	// string
	// string is a little complicated because it is a struct with
	// 'len and ptr' in it. and the ptr is immutable
	// in the following case, 'len and ptr' is copied during sending
	// to channel, but the byte array in it is not
	//
	// data4 += "b" created another byte array because it is immutable
	// so it will not affect the original byte array which is passed
	// in channel
	//
	// a possible way to change the byte array is using slice on it
	var c4 chan string
	c4 = make(chan string, 1)
	data4 := "a"
	fmt.Println("data sent to channel = ", data4)
	c4 <- data4
	data4 += "b"
	fmt.Println("and changed to ", data4)
	data4 = <-c4
	fmt.Println("data received from channel =", data4)

	pheader()
	// array
	// array is passed as VALUE, so the expectation is
	// we could receive the unchanged array from channel
	var c5 chan [5]int
	c5 = make(chan [5]int, 1)
	data5 := [5]int{1, 2, 3, 4, 5}
	fmt.Println("data sent to channel = ", data5)
	c5 <- data5
	data5[0] = 0
	fmt.Println("and changed to ", data5)
	data5 = <-c5
	fmt.Println("data received from channel =", data5)
}

func rangeClose() {
	// close() will not 'release resources', it is a kind
	// of notification from sender to receiver
	//
	// ONLY sender can close the channel, that means receiver can still
	//      work after close, but sender can't, will panic
	// ONLY used in case of sender want to notify receiver no more data
	//
	// reopen? impossible

	c0 := make(chan int)

	pheader()
	// receiver will blocked until close() is called, ok will be false
	c1 := make(chan int)
	go func() {
		i, ok := <-c1
		fmt.Println(i, ok)
		c0 <- 0 // goroutine done
	}()

	close(c1)
	// c1 <- 1 // will panic here
	<-c0 // wait goroutine done

	pheader()
	// range read
	c2 := make(chan int)
	go func() {
		// for channel range, only one return value for each call
		// 'i,ok := range c' is invalid
		for i := range c2 {
			fmt.Println("received ", i)
		}
		c0 <- 0
	}()

	for i := 0; i < 5; i++ {
		c2 <- i
	}
	close(c2)
	<-c0
}

func multipleSenderReceivers() {
	// one sender, multiple receivers
	// the result depends on:
	// - logic in reader, if there's a sleep in range loop, then
	//   5 goroutines will be scheduled equally
	// - buffer size of channel, if the buffer is more than 5, only
	//   first goroutine will be scheduled, others will be starv
	//   if buffer size is less than 5, result is prety random, but
	//   anyway the first goroutine seems always get more chance
	//
	// conclusion: DON'T DO THIS
	c := make(chan int)
	var w sync.WaitGroup
	w.Add(5)

	for i := 1; i <= 5; i++ {
		go func(i int, ci chan int) {
			j := 1
			for v := range ci {
				fmt.Printf("%d.%d got %d\n", i, j, v)
				j += 1
			}
			w.Done()
		}(i, c)
	}

	for i := 1; i <= 25; i++ {
		c <- i
	}
	close(c)
	w.Wait()
}

func selectOnChannel() {
	c0 := make(chan int)
	c1 := make(chan int)
	c2 := make(chan int)

	go func() {
		// select on receiver
		for {
			select {
			case i, ok := <-c0:
				fmt.Println("get data from c0", i, ok)
			case <-c1:
				fmt.Println("get data from c1")
			case <-c2:
				fmt.Println("get data from c2")
				break
			default:
				// it will be executed if all other branches are blocked
				// so if the following print is not commented, it will
				// be executed very fast
				//fmt.Println("default")
			}
		}
	}()

	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		if i%2 != 0 {
			c0 <- i
		} else {
			c1 <- i
		}
	}
	//close(c0)
	c2 <- 1
}

func channelTest() {

	pheader()
	// non-buffered channel, see bufferedOrNot() for detail
	bufferedOrNot(0, make(chan int))

	pheader()
	// buffered channel, see bufferedOrNot() for detail
	bufferedOrNot(1, make(chan int, 1))

	// how data types passed thru channels
	channelTypes()

	// how range and close works
	rangeClose()

	pheader()
	// sender:receiver  n:m
	multipleSenderReceivers()

	pheader()
	// select
	selectOnChannel()
}

func init() {
	pheader = hello.GetTestCaseHeaderFunc()
}
