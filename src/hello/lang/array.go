package lang

import (
	"fmt"
	"hello"
)

func arrayAsParam(args [5]int) {
	fmt.Println("\tchanged element 1 to 100")
	args[1] = 100
}

func arrayPointerAsParam(args *[5]int) {
	fmt.Println("\tchanged element 1 to 100")
	args[1] = 100
}

func arrayTest() {
	pheader := hello.GetTestCaseHeaderFunc()

	pheader()
	// array declare 1
	var a1 [5]int // initialize is not must
	a1[0] = 4
	a1[1] = 3
	a1[2] = 2
	a1[3] = 1
	a1[4] = 0
	fmt.Println("length of a1 is ", len(a1))
	for i, v := range a1 {
		fmt.Printf("\ta1[%d] = %d\n", i, v)
	}

	pheader()
	// array declare 2
	var a2 = [...]int{0, 1, 2, 3, 4} // initialize is must
	fmt.Println("length of a2 is ", len(a2))
	fmt.Println("a2 = ", a2)

	pheader()
	// new array pointer
	var a3 = new([5]int) // it is a pointer to array
	a3[0] = 4
	a3[1] = 3
	a3[2] = 2
	a3[3] = 1
	a3[4] = 0
	// length of a3 still works
	fmt.Println("length of a3 is ", len(a3))
	fmt.Println("a3 = ", a3)

	pheader()
	// init partial elements
	var a4 = [5]int{4: 100}
	fmt.Println("a4 = ", a4)

	pheader()
	// copy array
	var a5 = a4
	fmt.Println("\tchanged a5[0] to 99")
	a5[0] = 99
	fmt.Println("a4 = ", a4)
	fmt.Println("a5 = ", a5)
	// can't assigne a3 to a5(e.g. a5 = a3) because a3 is a pointer

	pheader()
	// compare two array
	fmt.Println("a1 = ", a1)
	fmt.Println("a2 = ", a2)
	fmt.Println("a3 = ", a2)
	fmt.Println("a1 == a2 ? ", a1 == a2)
	// a1 can't compare with a3, because a3 is a pointer

	pheader()
	// array as func parameter
	fmt.Println("before arrayAsParam()")
	fmt.Println(a1)
	arrayAsParam(a1)
	fmt.Println("after arrayAsParam()")
	fmt.Println(a1)

	pheader()
	// array pointer as func parameter
	fmt.Println("before arrayPointerAsParam()")
	fmt.Println(a3)
	arrayPointerAsParam(a3)
	fmt.Println("after arrayPointerAsParam()")
	fmt.Println(a3)
}
