package lang

import (
	"fmt"
	"hello"
	"runtime"
	"time"
)

var (
	go1Ret = 0
	go2Ret = 0
)

// return a fibonacci function
//
// sum is here as parameter to give us a chance to observe the progress
// of the recursive calls
func makeFib(sum *int) func(int) int {
	var fib func(int) int
	fib = func(n int) int {
		if n == 2 {
			return 1
		}
		if n == 1 {
			return 1
		}
		*sum++
		return fib(n-1) + fib(n-2)
	}

	return fib
}
func go1() {
	fib := makeFib(&go1Ret)
	fib(50)
	fmt.Println("go1 is exiting ..")
}

func go2() {
	fib := makeFib(&go2Ret)
	fib(50)
	fmt.Println("go2 is exiting ..")
}

// wait for a short time(before fib(50) returns) and then
// observe the progress of each goroutine
// in Go 1.1, go2() will be executed only after go1() complete.
// in Go 1.2+, go2() and go1() will be scheduled equally, we
// should observe that both of them have progress
func blocking() {
	time.Sleep(5 * time.Second)
	fmt.Println("go1 state is ", go1Ret)
	fmt.Println("go2 state is ", go2Ret)
}

func routineTest() {
	pheader := hello.GetTestCaseHeaderFunc()

	pheader()
	// this test is going to demostrate, even if the goroutine doesn't
	// call runtime.GoSched() and any other system calls, it still
	// has chance to be scheduled

	// one processor
	runtime.GOMAXPROCS(1)
	go go1()
	go go2()

	blocking()
}
