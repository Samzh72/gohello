package lang

import (
	"hello"
)

type LangTestSuite struct {
	hello.HelloTestSuite
}

func New() *LangTestSuite {
	ts := new(LangTestSuite)
	ts.Init()

	ts.RegisterKeyword("array", arrayTest)
	ts.RegisterKeyword("slice", sliceTest)
	ts.RegisterKeyword("map", mapTest)
	ts.RegisterKeyword("defer", deferTest)
	ts.RegisterKeyword("panic", panicTest)
	ts.RegisterKeyword("goroutine", routineTest)
	ts.RegisterKeyword("channel", channelTest)

	return ts
}
