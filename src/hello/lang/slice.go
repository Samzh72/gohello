package lang

import (
	"fmt"
	"hello"
	"reflect"
)

func sliceTest() {
	pheader := hello.GetTestCaseHeaderFunc()

	pheader()
	// make slice from array
	var a1 [5]int  // initialize is not must
	var s1 = a1    // s1 is an array, not slice
	var s2 = a1[:] // s2 is a slice
	a1[0] = 4
	a1[1] = 3
	a1[2] = 2
	a1[3] = 1
	a1[4] = 0
	fmt.Println("length of a1 is ", len(a1))
	fmt.Println("length of s1 is ", len(s1))
	fmt.Println("cap of s1 is ", cap(s1))
	fmt.Println("a1 = ", a1)
	fmt.Println("s1 = ", s1)

	fmt.Println("type of s1 is ", reflect.TypeOf(s1).String())
	fmt.Println("type of s2 is ", reflect.TypeOf(s2).String())

	pheader()
	// make slice from array 2
	var s3 = a1[0:5] // lo:hi, from a1[lo] to a1[hi-1]
	fmt.Println("length of s3 is ", len(s3))
	fmt.Println("cap of s3 is ", cap(s3))

	pheader()
	// declear slice and initialize
	var s4 = []int{0, 1, 2, 3, 4}
	fmt.Println("length of s4 is ", len(s4))
	fmt.Println("cap of s4 is ", cap(s4))

	pheader()
	// declare nil slice
	var s5 []int // slice is not a value, it is nil before initialized
	fmt.Println("s5 = ", s5)
	fmt.Println("s5 == nil ? ", s5 == nil)
	fmt.Println("length of s5 is ", len(s5))
	fmt.Println("cap of s5 is ", cap(s5))

	pheader()
	// new a slice, never do this
	var s6 = new([]int) // slice is not a value, it is nil before initialized
	fmt.Println("s6 = ", s6)
	fmt.Println("s6 == nil ? ", s6 == nil)
	fmt.Println("*s6 == nil ? ", *s6 == nil)
	fmt.Println("length of *s6 is ", len(*s6))
	fmt.Println("cap of *s6 is ", cap(*s6))

	pheader()
	// change content
	var s7 = a1[:]
	fmt.Println("a1 = ", a1)
	s7[0] = 100
	s7[1] = 101
	s7[2] = 102
	s7[3] = 103
	s7[4] = 104
	fmt.Println("length of a1 is ", len(a1))
	fmt.Println("cap of a1 is ", cap(a1))
	fmt.Println("length of s7 is ", len(s7))
	fmt.Println("cap of s7 is ", cap(s7))
	fmt.Println("a1 = ", a1)
	fmt.Println("s7 = ", s7)

	pheader()
	// increase capacity within original array
	var s8 = a1[:0] // empty slice, a1 length is 5
	fmt.Println("s8 == nil ? ", s8 == nil)
	fmt.Println("a1 = ", a1)
	s8 = append(s8, 10, 11, 12, 13, 14)
	fmt.Println("length of a1 is ", len(a1))
	fmt.Println("cap of a1 is ", cap(a1))
	fmt.Println("length of s8 is ", len(s8))
	fmt.Println("cap of s8 is ", cap(s8))
	fmt.Println("a1 = ", a1)
	fmt.Println("s8 = ", s8)

	pheader()
	// increase capacity beyond original array
	// if slice is created from an array, it's better never do this
	var s9 = a1[:1] // 1 element slice, a1 length is 5
	fmt.Println("a1 = ", a1)
	s9 = append(s9, 20, 21, 22, 23, 24, 25)
	fmt.Println("length of a1 is ", len(a1))
	fmt.Println("cap of a1 is ", cap(a1))
	fmt.Println("length of s9 is ", len(s9))
	fmt.Println("cap of s9 is ", cap(s9))
	fmt.Println("a1 = ", a1)
	fmt.Println("s9 = ", s9)

	pheader()
	// make a slice
	var s10 = make([]int, 5, 5) // length = 5, cap = 5
	fmt.Println("s10 = ", s10)

	pheader()
	// copy slice
	copy(s10, s9) // only copy part of s9, because s9 is too long
	fmt.Println("s10 = ", s10)
	fmt.Println("s9 = ", s9)

	// shrink slice
	// NO-WAY, reallocate a smaller one and wait for GC
}
