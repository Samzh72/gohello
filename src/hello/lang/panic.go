package lang

import (
	"fmt"
	"hello"
	"reflect"
)

func nonRecoverDeferFunc(i int) {
	fmt.Println("none of my business, I'm just normal defer ", i)
}

func recoverDeferFunc() {
	// recover will catch a panic here
	if r := recover(); r != nil {
		fmt.Println("Recovered in f", r)
		fmt.Println("   type of panic is ", reflect.TypeOf(r))
	}

	// recover will return nil here
	if r := recover(); r == nil {
		fmt.Println("nothing to recover anymore")
	}

}

func iAmGoingToPanic(obj interface{}) {
	defer nonRecoverDeferFunc(0)
	defer recoverDeferFunc()
	defer nonRecoverDeferFunc(1)
	fmt.Println("hurt... going to panic")
	panic(obj)
}

func changeReturnValueDuringRecover(r int) (i int) {
	defer func() {
		if r := recover(); r != nil {
			i = r.(int) // type assertion
		}
	}() // it has to be a function call

	fmt.Println("hurt... going to panic")
	panic(r)

	// will never goes here
	return 0
}

func invalidDefer() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println(r)
		}
	}()
	// recover() must be called in function
	// here nil will be printed, that means nothing catched
	defer fmt.Println(recover())
	panic("hello")
}

func panicTest() {
	pheader := hello.GetTestCaseHeaderFunc()

	pheader()
	// panic string
	iAmGoingToPanic("string")

	pheader()
	// panic int
	iAmGoingToPanic(10)

	pheader()
	// panic pointer
	type sp struct {
		i int
	}
	iAmGoingToPanic(new(sp))

	pheader()
	// panic nil, recover can't catch nil
	iAmGoingToPanic(nil)

	pheader()
	// change return value during recover
	fmt.Println("expect 0, and get ", changeReturnValueDuringRecover(0))
	fmt.Println("expect 1, and get ", changeReturnValueDuringRecover(1))

	pheader()
	// invalid defer
	invalidDefer()

}
