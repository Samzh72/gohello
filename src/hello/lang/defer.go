package lang

import (
	"fmt"
	"hello"
)

func deferFunc(i int) {
	fmt.Println("defer ", i)
}

func deferTest() {
	pheader := hello.GetTestCaseHeaderFunc()

	pheader()

	// i++
	var i int
	fmt.Println("step ", i)
	i++
	defer deferFunc(i)
	i++
	defer deferFunc(i)

	// i++
	fmt.Println("step ", i)
	i++

	var s []int
	if s == nil {
		// i++
		fmt.Println("step ", i)
		i++

		defer deferFunc(i)
		i++
		defer deferFunc(i)

		// i++
		fmt.Println("step ", i)
		i++
	}
	// i ++
	fmt.Println("step ", i)
	i++
}
