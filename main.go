package main

import (
	"flag"
	"fmt"
	"hello"
	"hello/lang"
	"hello/perf"
	"hello/pkg"
	"os"
	"runtime/pprof"
	"utils"
)

func help() {
	fmt.Println("gohello -p|-l|-k [keyword]")
	fmt.Println("\t-p: performance playground")
	fmt.Println("\t-l: language playground")
	fmt.Println("\t-k: standard package playground")
}

func main() {
	f, err := os.Create("pprof.txt")
	if err != nil {
		utils.LogFatal(err.Error())
	}
	pprof.StartCPUProfile(f)
	defer pprof.StopCPUProfile()

	utils.LogInfo("hello main")

	flag.Usage = help
	var p *string = flag.String("p", "", "performance playground")
	var l *string = flag.String("l", "", "language playground")
	var k *string = flag.String("k", "", "package playground")
	flag.Parse()

	if *p == "" && *l == "" && *k == "" {
		help()
		return
	}

	run := func(k string, ts hello.TestSuite) {
		if k == "all" {
			ts.ListKeywords()
			return
		} else {
			handle, ok := ts.GetHandlerByKeyword(k)
			if !ok {
				utils.LogError("invalid keyword")
				help()
				return
			}
			handle()
		}
	}

	if *p != "" {
		run(*p, perf.New())
	} else if *l != "" {
		run(*l, lang.New())
	} else if *k != "" {
		run(*k, pkg.New())
	}
}

func init() {
	utils.LogRegisterModule("MAIN", utils.LogLevelAll, true)
}
